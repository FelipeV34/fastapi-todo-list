from fastapi import FastAPI

app = FastAPI()


@app.get(path="/ping/", summary="Ping endpoint to test the server.", tags=["Ping"])
def ping() -> str:
    return "Pong"
